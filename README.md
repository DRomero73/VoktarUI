# VoktarUI: Minimalist / Functional Custom Interface for EverQuest

Features:

	. 14px high titlebars with custom minimize/maximize, help and close buttons.
	. 1px borders.
	. No filigrees! All window borders are square.
	. Unnecessary titlebars removed.
	. Detailed vertical layouts for the long and short duration buff windows.
	. 18*18px buff icons.
	. Play with Classic or Default Spell Icons.
	. Play with Transparent or the Default Windows.
	. Horizontal combat abilities window.
	. New effects window, where only the target spells and consider colors are visible.
	. Combined target and target of target windows.
	. Reworked casting and breath bars.
	. Smaller MainEQ button.
	. Compact pet window.
	. Compact player window.
	. Compass window with normal and alternate experience bars, character level and alternate advancement points.
	. Lots of cosmetic fixes.


Release notes:

	This is a very simple UI, nothing fancy i would say, except for a pair of modifications.

	My goal was to preserve the original look and feel and get rid of the things that annoy me the most,
	like filigrees, rounded corners and those big and intrusive titlebars that are everywhere
	wasting precious space.

	Now i think the windows are much easier to organize in screen, in most cases they are smaller and clearer.
	Some other windows are reworked too, hope you like the modifications.

	Please, keep in mind that this UI is optimized for the Windows Default Font "Segoe UI".

	There are several batch files for your use inside the VoktarUI-Live folder.
	You can use them, or instead replace by hand the individual files inside the VoktarUI-Live
	root folder with the ones located in the Alternate_Icons and Alternate_Windows folders.

	Here are the ".bat" files that will automate this job for you:

	. Spells_Icons_Classic.bat: Classic Era.
	. Spells_Icons_Default.bat: Default, Current Era.

	. Windows_Transparent.bat: Use some Transparent Windows.
	. Windows_Default.bat: Use the Default Windows.

	You have to reload the UI for the changes to take effect.

	If you want to try the windows layout as shown in the screenshots, copy the file "UI_VoktarUI_Solo.ini" to
	the game installation directory, then you can switch to it in-game via:

		"Options Window > General Tab > Copy Layout"

	For a complete list of changes, read "VoktarUI_Changelog.txt".


Recommended companion applications:

	WinEQ 2: http://www.lavishsoft.com/download/wineq2

		WinEQ 2 is free, it allows you to play in windowed mode and let you emulate a full screen window
		without borders.

		This come handy for controlling several game sessions simultaneously. Also allows you to play the
		game directly without updating it first, bypassing the launcher.

		Take note that you still have to update the game through the launcher after each patch day for the
		game to continue working.

	GINA: http://eq.gimasoft.com/gina/Download.aspx

		GINA is free, it's a monitor program that logs your EverQuest logfile searching for text triggers.
		When a trigger is found it pops up a text overlay on your EverQuest gaming window.

		You need to play in windowed mode for this to work, so WinEQ 2 is the perfect companion.